import React from 'react'
import { render } from 'react-dom'
import './styles.css'

class HelloReact extends React.Component {
  render() {
    return (
      <div id="hello-react">
        Hello from React!
      </div>
    )
  }
}

render(<HelloReact />, document.getElementById('root-app'))
