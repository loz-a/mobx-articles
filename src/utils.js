export const isElement = (element, type) =>
  element && element.type && element.type === type

export const isElementOneOf = (element, types) => {
  if (Array.isArray(types)) {
      for (let i = 0; i < types.length; i++) {
        if (isElement(element, types[i])) return true
      }
  } else {
    return isElement(element, types)
  }
  return false
}

export const getCurrentDate = (date = null) => {
  if (!date) date = new Date()
  return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
}

const isFunction = (func) => typeof func === 'function'

// inspired by https://atticuswhite.com/blog/render-if-conditionally-render-react-components/
export const ifExpr = (predicate) => (expr, elseExpr = null) =>
  predicate
    ? ( isFunction(expr) ? (expr.bind(this))() : expr )
    : ( isFunction(elseExpr) ? (elseExpr.bind(this))() : elseExpr)
