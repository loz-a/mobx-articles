import React from 'react'
import { render } from 'react-dom'
import { useStrict } from 'mobx';
import { Provider } from 'mobx-react';
import Store from './store'

import routes from './routes'

useStrict(true);
const store = new Store(window.INIT_STATE)

window.store = store

render(
  <Provider store={store}>
    {routes}
  </Provider>,
  document.getElementById('root-app')
)
