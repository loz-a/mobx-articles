import React from 'react'
import { Router, Route } from 'react-router'
import history from './history'
import Articles from './components/articles'
import AddArticle from './components/articles/article/add'
import EditArticle from './components/articles/article/edit'
import DeleteArticle from './components/articles/article/delete'
import ViewArticle from './components/articles/article/view'
import AddComment from './components/articles/article/comments/add-comment'

export default (
  <Router history={history}>
    <Route path="/(filter/:status)" component={Articles}>
      <Route path="article">
        <Route path="add" component={AddArticle} />
        <Route path="edit/:id" component={EditArticle} />
        <Route path="delete/:id" component={DeleteArticle} />
        <Route path=":id" component={ViewArticle}>
          <Route path="add-comment" component={AddComment} />
        </Route>
      </Route>
    </Route>
  </Router>
)
