import { computed, action } from 'mobx'
import { normalize } from 'normalizr'
import articleSchema from './schema'
import Articles from './collections/articles'
import Authors from './collections/authors'
import Comments from './collections/comments'

export default function(INIT_STATE = {}) {
  const initState = normalize(INIT_STATE, articleSchema).entities

  const articles = Articles.fromJS(initState.articles)
  const authors = Authors.fromJS(initState.authors)
  const comments = Comments.fromJS(initState.comments)

  // Articles
  this.articles = computed(() =>
    articles.data.values()
      .map((article) => ({
           ...article.toJS,
           author: authors.getById(article.author).toJS
      }))
  )

  this.articlesSize = () => articles.size
  this.getArticleById = (id) => articles.getById(id)
  this.saveArticle = (data) => articles.save(data)
  this.removeArticle = (id) => articles.remove(id)
  this.hasArticle = (id) => articles.data.has(id)
  this.getArticleComments = (id) => {
    const commentsIds = articles.data.get(id).comments
      .slice()
      .reduce((acc, id) => {
        acc[id] = true
        return acc
      }, {})

    return this.comments.get().filter((comment) => comment.id in commentsIds )
  }

  // Authors
  this.authors = computed(() => Object.values(authors.toJS))
  this.getAuthorById = (id) => authors.getById(id)

  // Comments
  this.comments = computed(() =>
    comments.data
      .map((comment) => ({
        ...comment.toJS,
        author: authors.getById(comment.author).toJS
      }))
  )

  this.commentsSize = () => comments.size

  this.saveComment = action((data) => {
    const article = articles.getById(data.articleId)
    if (!article) throw new Error(`Article with id: ${data.articleId} not exists in store`)
    articles.save({
      ...article,
      comments: [ ...article.comments, data.id ]
    })
    return comments.save(data)
  })
}
