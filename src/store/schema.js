import { Schema, arrayOf } from 'normalizr'

const article = new Schema('articles', { defaults: { comments: [] } })
const author = new Schema('authors')
const comment = new Schema('comments')

comment.define({
  author: author
})

article.define({
  author: author,
  comments: arrayOf(comment)
})

export default arrayOf(article)
