import { observable, computed, action, map } from 'mobx'
import Article from '../entities/article'
import { getCurrentDate } from '../../utils'

export default class Articles {
  @observable data = map()

  getById = (id) => this.data.has(id) ? this.data.get(id).toJS : null

  @action save = (data) => {
    const id = data.id || null

    data.updated = getCurrentDate()

    if (this.data.has(id)) this.data.get(id).populate(data)
    else this.data.set(id, Article.fromJS(data))
    return this.data.get(id).toJS
  }

  @action remove = (id) => {
    const result = (this.data.has(id)) ? this.data.get(id).toJS : false
    if (!result) return result
    this.data.delete(id)
    return result
  }

  @computed get toJS() {
    const articles = this.data
    return articles.keys().reduce((acc, id) => {
      const article = articles.get(id).toJS
      acc[id] = article
      return acc
    }, {})
  }

  @computed get size() {
    return this.data.size
  }

  @action static fromJS(data) {
    const store = new Articles()
    Object.keys(data).map((id) => {
      store.data.set(id, Article.fromJS(data[id]))
    })
    return store
  }
}
