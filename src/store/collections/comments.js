import { observable, computed, action } from 'mobx'
import Comment from '../entities/Comment'

export default class Comments {
  @observable data = []

  @action save = (data) => {
    const comment = new Comment(data.id, data.text, data.author)
    this.data.push(comment)
  }

  @computed get toJS() {
    const comments = this.data
    return comments.reduce((acc, comment) => {
      acc[comment.id] = comment.toJS
      return acc
    }, {})
  }

  @computed get size() {
    return this.data.length
  }

  @action static fromJS(data) {
    const store = new Comments()
    Object.keys(data).map((id) => {
      store.data.push(Comment.fromJS(data[id]))
    })
    return store
  }
}
