import { observable, computed, action } from 'mobx'
import Author from '../entities/Author'

export default class Authors {
  @observable data = {}

  getById = (id) => (id in this.data) ? this.data[id] : null

  @action static fromJS(data) {
    const store = new Authors()
    Object.keys(data).map((id) => {
      store.data[id] = Author.fromJS(data[id])
    })
    return store
  }

  @computed get toJS() {
    const authors = this.data
    return Object.keys(authors).reduce((acc, id) => {
      acc[id] = authors[id].toJS
      return acc
    }, {})
  }

  @computed get size() {
    return Object.keys(this.data).length
  }
}
