import { observable, computed, action, autorun } from 'mobx'

export default class Article {
  id
  @observable title
  @observable text
  @observable updated
  @observable author
  @observable isPublic
  @observable comments

  @action populate = (data) => {
    if ('id' in data) this.id = data.id
    if ('title' in data) this.title = data.title
    if ('text' in data) this.text = data.text
    if ('updated' in data) this.updated = data.updated
    if ('author' in data) this.author = data.author
    if ('isPublic' in data) this.isPublic = JSON.parse(data.isPublic)
    if ('comments' in data) this.comments = data.comments
  }

  @computed get toJS() {
    return {
      id: this.id,
      title: this.title,
      text: this.text,
      updated: this.updated,
      author: this.author,
      isPublic: this.isPublic,
      comments: this.comments
    }
  }

  @action static fromJS(data) {
    const article = new Article()
    article.populate(data)
    return article
  }
}
