
export default class Comment {
  id
  text
  author

  constructor(id, text, author) {
    this.id = id
    this.text = text
    this.author = author
  }

  get toJS() {
    return {
      id: this.id,
      text: this.text,
      author: this.author
    }
  }

  static fromJS(data) {
    return new Comment(data.id, data.text, data.author)
  }

}
