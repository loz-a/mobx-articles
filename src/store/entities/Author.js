import { observable, computed, action } from 'mobx'

export default class Author {
  id
  @observable name

  constructor(id, name) {    
    this.id = id
    this.name = name
  }

  @computed get toJS() {
    return {
      id: this.id,
      name: this.name
    }
  }

  @action static fromJS(data) {
    return new Author(data.id, data.name)
  }
}
