import React, { PropTypes, cloneElement } from 'react'
import { Link } from 'react-router'
import { observer } from 'mobx-react'
import { isElementOneOf, ifExpr } from '../../../utils'
import EditArticle from './edit'
import DeleteArticle from './delete'
import ViewArticle from './view'
import { STATUS_ALL, STATUS_PUBLIC, STATUS_DRAFT } from '../status-filters'

@observer
class Article extends React.Component {

  validateFilterStatus = () => {
    const { status = STATUS_ALL, article } = this.props
    if (status === STATUS_ALL) return true
    
    const isPublicStatus = (status === STATUS_PUBLIC) ? true : false
    return isPublicStatus === article.isPublic
  }

  render() {
    const { children, article, authors, remove, save } = this.props
    if (!this.validateFilterStatus()) return null

    const linkTo = (save) ? "/" : `/article/${article.id}`
    const ifChildrenIsViewArticle = ifExpr(isElementOneOf(children, ViewArticle))
    const ifChildrenIsEditDelArticle = ifExpr(isElementOneOf(children, [EditArticle, DeleteArticle])).bind(this)

    return (
      <li>
        <article>
          <header>
            <h3>
              <Link to={linkTo}>{article.title}</Link>
              <br/>Author: {article.author.name}
            </h3>
          </header>

          {ifChildrenIsViewArticle(
              () => cloneElement(children, { article })
            )
          }

          {ifChildrenIsEditDelArticle(
            () => cloneElement(children, { article, remove, save, authors })
          )}

        </article>
      </li>
    )
  }

  static propTypes = {
    article: PropTypes.any.isRequired,
    authors: PropTypes.array.isRequired,
    children: PropTypes.element,
    remove: PropTypes.func,
    save: PropTypes.func,
    status: PropTypes.string
  }

}
export default Article
