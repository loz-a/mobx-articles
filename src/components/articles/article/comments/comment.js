import React, { PropTypes } from 'react'

let Comment = ({
    comment: { id, text, author }
}) => (
    <article>
        <header>
            <h5>
                Comment No:{id}, Posted By: {author.name}
            </h5>
        </header>
        <p>
            {text}
        </p>
    </article>
)

Comment.propTypes = {
    comment: PropTypes.shape({
        id: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        author: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }).isRequired
    })
}

export default Comment
