import React, { PropTypes, cloneElement } from 'react'
import { Link } from 'react-router'
import { observer, inject } from 'mobx-react'
import Comment from './comment'
import AddComment from './add-comment'
import { isElement, ifExpr } from '../../../../utils'

@inject('store')
@observer
class Comments extends React.Component {
  comments

  componentWillMount() {
    const { store, article } = this.props
    this.comments = store.getArticleComments(article.id)
  }

  componentWillReceiveProps(newProps) {
    const { store, article } = newProps
    this.comments = store.getArticleComments(article.id)
  }

  render() {
    const { children, article, store: { saveComment: save } } = this.props

    const ifChildrenIsAddComment = ifExpr(isElement(children, AddComment)).bind(this)

    return (
      <section>
        {ifChildrenIsAddComment(
          () => cloneElement(children, { article, save }),
          () => (
            <p>
              <Link to={`/article/${article.id}/add-comment`}>Add comment</Link>
            </p>
          )
        )}
        {this.getComments()}
      </section>
    )
  }

  getComments = () => {
    if (!this.comments.length) return null
    return (
      <div>
        <h4>
            Comments:
        </h4>
        {this.comments.reverse().map((comment) => (
          <Comment key={comment.id} comment={comment} />
        ))}
      </div>
    )
  }

  static propTypes = {
    store: PropTypes.any.isRequired,
    article: PropTypes.any.isRequired
  }
}

export default Comments
