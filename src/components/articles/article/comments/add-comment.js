import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import history from '../../../../history'

@observer
class AddComment extends React.Component {

  @observable text

  render() {
    const { article } = this.props
    return (
      <form onSubmit={this.handleSubmit}>
        <p>
          <label>
            Input comment:
            <br/>
            <textarea
              rows="2"
              cols="80"
              onChange={this.handleChange} />
          </label>
        </p>

        <p>
          <input type="submit" defaultValue="Ok" />
          <Link to={`/article/${article.id}`}>Cancel</Link>
        </p>
      </form>
    )
  }

  @action handleChange = (evt) => {
    this.text = evt.target.value
  }

  handleSubmit = (evt) => {
    evt.preventDefault()

    const { save, article } = this.props
    save({
      id: JSON.stringify(Date.now()),
      text: this.text,
      author: article.author.id,
      articleId: article.id
    })
    history.replace(`/article/${article.id}`)
  }

  static propTypes = {
    article: PropTypes.any,
    save: PropTypes.func
  }
}

export default AddComment
