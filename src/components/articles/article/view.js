import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import Comments from './comments'

const ViewArticle = ({ article, children }) => {
  if (!article) return null
  return (
    <section>
      <ul>
        <li><Link to={`/article/edit/${article.id}`}>[edit]</Link></li>
        <li><Link to={`/article/delete/${article.id}`}>[delete]</Link></li>
      </ul>

      {/* {loader} */}
      <p>
        {article.text}
      </p>
      <p>
        Last updated: {article.updated}
      </p>

      <Comments article = {article} children={children} />
    </section>
  )
}

ViewArticle.propTypes = {
  article: PropTypes.any,
  children: PropTypes.element
}

export default ViewArticle
