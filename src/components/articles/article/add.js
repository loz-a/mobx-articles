import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import history from '../../../history'

@observer
class AddArticle extends React.Component {

  @observable id
  @observable title
  @observable text
  @observable isPublic = true
  @observable author

  constructor(props) {
    super(props)
    const { article } = props
    if (article) this.populate(article)
  }

  componentWillReceiveProps(nextProps) {
    this.populate(nextProps.article)
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <p>
          <label>
            Title:
            <input value={this.title} onChange={this.handleChange('title')} />
          </label>
        </p>

        <p>
          <label>
            Text:
            <textarea
              rows="6"
              cols="80"
              value={this.text}
              onChange={this.handleChange('text')} />
          </label>
        </p>

        {this.renderAuthor()}

        <p>
          <label>
            isPublic:
            <input
              type="checkbox"
              defaultChecked={this.isPublic}
              onChange={this.handleChange('isPublic')} />
          </label>
        </p>

        <p>
          <input type="submit" defaultValue="Ok"/>
          <Link to="/">Cancel</Link>
        </p>
      </form>
    )
  }

  renderAuthor() {
    if (this.id) return (
      <p>
        <label>
          Author:
          <input readOnly defaultValue={this.author.name} />
        </label>
      </p>
    )

    return (
      <p>
        <label>Author: </label>
        <select onChange={this.handleChangeAuthor}>
          {this.props.authors.map((author) => {
            return (
              <option key={author.id} value={author.id}>{author.name}</option>
            )
          })}
        </select>
      </p>
    )
  }

  handleChange = (input) => action((evt) => {
    const target = evt.target
    this[input] = target.type == 'checkbox' ? target.checked : target.value
  })

  @action handleChangeAuthor = (evt) => {
    const selectedId = evt.target.value
    this.author = this.props.authors.filter((author) => author.id === selectedId)[0]
  }

  handleSubmit = (evt) => {
    evt.preventDefault()
    this.props.save({
      id: (this.id) ? this.id : JSON.stringify(Date.now()),
      title: this.title,
      text: this.text,
      isPublic: this.isPublic,
      author: (this.author) ? this.author.id : this.props.authors[0].id
    })
    history.replace('/')
  }

  @action populate(article) {
    this.id = article.id,
    this.title = article.title,
    this.text = article.text,
    this.isPublic = article.isPublic,
    this.author = article.author
  }

  static propTypes = {
      article: PropTypes.any,
      authors: PropTypes.array,
      save: PropTypes.func
  }
}

export default AddArticle
