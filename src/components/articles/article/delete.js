import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import history from '../../../history'

const DeleteArticle = ({ article, remove }) => {

  const deleteHandler = (evt) => {
    evt.preventDefault()
    remove(article.id)
    history.replace('/')
  }

  return (
    <div>
      Are you sure you want to delete this article?
      <Link to="/" onClick={deleteHandler}>Yes</Link>
      <Link to="/">No</Link>
    </div>
  )
}

DeleteArticle.propTypes = {
  article: PropTypes.any,
  remove: PropTypes.func
}

export default DeleteArticle
