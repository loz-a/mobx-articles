import React from 'react'
import { Link } from 'react-router'

export const STATUS_ALL = 'all'
export const STATUS_PUBLIC = 'public'
export const STATUS_DRAFT = 'draft'

const StatusFilters = () => (
  <ul>
    <li><Link to="/">{STATUS_ALL}</Link></li>
    <li><Link to={`/filter/${STATUS_PUBLIC}`}>{STATUS_PUBLIC}</Link></li>
    <li><Link to={`/filter/${STATUS_DRAFT}`}>{STATUS_DRAFT}</Link></li>
  </ul>
)

export default StatusFilters
