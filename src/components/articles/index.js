import React, { PropTypes, cloneElement } from 'react'
import { Link } from 'react-router'
import { observer, inject } from 'mobx-react'
import { isElementOneOf, ifExpr } from '../../utils'
import Article from './article'
import AddArticle from './article/add'
import EditArticle from './article/edit'
import DeleteArticle from './article/delete'
import ViewArticle from './article/view'
import StatusFilters from './status-filters'

@inject('store')
@observer
class Articles extends React.Component {

  articles
  authors

  componentWillMount() {
    this.articles = this.props.store.articles.get()
    this.authors = this.props.store.authors.get()
  }

  componentWillReceiveProps(newProps) {
    this.articles = newProps.store.articles.get()
    this.authors = newProps.store.authors.get()
  }

  render() {
    const { store, params, children } = this.props
    const { id: paramsId, status } = params
    const { saveArticle: save, removeArticle: remove } = store

    if (!store.articlesSize()) return null

    const ifChildrenIsAddArticle = ifExpr(isElementOneOf(children, AddArticle)).bind(this)
    const isChildrenIsEditDelViewArticle = isElementOneOf(children, [EditArticle, DeleteArticle, ViewArticle])

    return (
      <section>
        <h1>Last Articles:</h1>
        <StatusFilters />

        <ul>
          {this.articles.map((article) =>
            ifExpr((paramsId === article.id) && isChildrenIsEditDelViewArticle)(
              () => <Article key={article.id} {...{ article, children, save, remove, status }} authors={this.authors} />,
              () => <Article key={article.id} article={article} authors={this.authors} status={status} />
            )
          )}
        </ul>

        {ifChildrenIsAddArticle(
          () => cloneElement(children, { save: store.saveArticle, authors: this.authors}),
          () => <Link to="/article/add">Add Article</Link>
        )}

      </section>
    )
  }

  static propTypes = {
      store: PropTypes.any.isRequired
  }
}

export default Articles
