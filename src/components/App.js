import React from 'react'
import { observer } from 'mobx-react';
import Articles from './articles'

@observer
class App extends React.Component {

  render() {

    return (
      <Articles />
    )
  }

}

export default App
